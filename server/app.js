var express = require("express");

var app = express();

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));


var students = [
    
        {
    
            id: 1,
    
            name: "Kenneth",
    
            age:25
    
        },
    
        {
    
            id: 2,
    
            name: "Alex",
    
            age: 26
    
        }
    
    ];
    
    
    
    app.get("/students", (req,res)=>{
    
        console.log("I am in students endpoint");
    
        res.status(200).json(students);
    
    });
    
    
    
    app.get("/students/:student_id", (req,res)=>{
    
        console.log(req.params.student_id);
    
        let studentId = req.params.student_id;
    
        let studentObj = null;
    
        for(var x =0 ; x < students.length; x++){
    
            if(students[x].id == studentId){
    
                studentObj = students[x];
    
                break;
    
            }
    
        }
    
        res.status(200).json(studentObj);
    
    });
    
    
    
    app.use((req,res)=>{
    
        var x = 6;
    
        try{
    
            res.send("<h1> Oopps wrong please </h1>");
    
        }catch(error){
    
            console.log(error);
    
            res.status(500).send("ERROR");
    
        } 
    
    });


/*
app.get("/students/:student_id", (req,res)=>{

    console.log("I am in student endpoint");
    console.log("I am in student endpoint");

    var students = [
        {
        name: "kenneth",
        age: 25
         },
        {
        name: "alex",
         age: 28
         }

    ]
    res.status(200).json(students);
});


app.get("/students/:student_id", (req,res)=>{
    
        console.log(req.params.student_id);
    
        let studentId = req.params.student_id;
    
        let studentObj = null;
    
        for(var x =0 ; x < students.length; x++){
    
            if(students[x].id == studentId){
    
                studentObj = students[x];
    
                break;
    
            }
    
        }
    
        res.status(200).json(studentObj);
    
    });

app.use((req,res)=>{
    console.log("Nothing Found");
    res.send("<h1>Oops Nothing Found</h1>");


})

*/



app.listen(NODE_PORT, ()=>{
    console.log(`web App started at ${NODE_PORT}`);

})